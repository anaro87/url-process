package urlprocess.urlprocess;

import java.io.IOException;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
//url
import org.apache.http.HttpHost;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.conn.DefaultProxyRoutePlanner;
import java.util.Properties;  
//mail
import javax.mail.*;    
import javax.mail.internet.*;
//file
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVPrinter;
//xml
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.DocumentBuilder;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
//salesforce
import com.sforce.ws.ConnectorConfig;
import org.w3c.dom.Node;
import org.w3c.dom.Element;
import java.io.File;

public class App {
	static Connection Conn = null;
	static Statement Stmt = null;
	static ResultSet Resultset = null;


	/**
    |--------------------------------------------------------------------------
    |@method main
    |--------------------------------------------------------------------------
    |@description this method have the core logic connect to telus proxy, connect to the database, get links info, save link info, send notifications
    |@created  07/11/2017
    |@createdBy x188421
    |@exception
    |@return void
    */
	public static void main(String[] args)
			throws IOException, KeyManagementException, NoSuchAlgorithmException, KeyStoreException 
	{
		long startTime;
		long elapseTime;
		String insertString, msg = "", html = "", sfpws="";
		Map<String, String> urlMap;
		CloseableHttpClient httpclient;
		//getting current date
		DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		Date curDate = new Date();
		java.sql.Timestamp sqlDate = new java.sql.Timestamp(curDate.getTime());

		System.out.println("<-- Process Start... -->");
		Map<String, String> config = new HashMap<String, String>();
		config.putAll(getConfigInfo()); 
		
		System.out.println("1. Please verify that you are inside of TELUS VPN...");

		try {
			HttpHost proxy = new HttpHost(config.get("proxy_ip"), Integer.parseInt(config.get("proxy_port"))); // telus proxy
			DefaultProxyRoutePlanner routePlanner = new DefaultProxyRoutePlanner(proxy);
			List<Map<String, String>> urlsList = getUrls();
			//adding salesforce url
			if(config.get("sf_test_con").equals("yes"))
			{
				urlMap = new HashMap<String, String>();
				urlMap.put("url",config.get("sf_url"));
				urlMap.put("isServer","no");
				urlsList.add(urlMap);
			}
			
			try {
				// Attempts to establish a connection
				// dbc:oracle:thin:@HOST:PORT:SID,usr,pws
				Conn = DriverManager.getConnection("jdbc:oracle:thin:@"+config.get("db_hostname")+":"+config.get("db_port")+":"+config.get("db_servicename"),
						config.get("db_user"), config.get("db_pws"));
				System.out.println("2. Database connection created...");
				// Returns the Class object associated with the class
				Class.forName("oracle.jdbc.driver.OracleDriver");
				DriverManager.setLoginTimeout(5);
			} catch (ClassNotFoundException exception) {
				System.out.println("Oracle Driver Class Not found Exception: " + exception.toString());
				//create error file
				createCsvFile("UrlInfoErrorLog.csv", exception.toString() + " " + exception.getMessage(), "", dateFormat.format(curDate.getTime()));
				//send error notification
				sendErrornotification(exception.toString() + " " + exception.getMessage(), config.get("email_subject"));
				return;
			}
			System.out.println("3. Getting urls information... ");
			
			for (int i = 0; i < urlsList.size(); i++) 
			{
				urlMap = new HashMap<String, String>();
				urlMap.putAll(urlsList.get(i)); 
				if(urlMap.get("url").contains("forcecomHomepage.apexp") && config.get("sf_test_con").equals("yes"))
				{
					sfpws = config.get("sf_pws").concat(config.get("sf_security_token"));
					elapseTime = connectToSalesforce(config.get("sf_username"), sfpws, config.get("sf_url"), config.get("proxy_ip"), Integer.parseInt(config.get("proxy_port")));
				}
				if(urlMap.get("isServer").equals("yes")){
					//is an internal server
					httpclient = HttpClients.createDefault();
				}else{
					//is an external link so we need to used the proxy to connect
					httpclient = HttpClients.custom().setRoutePlanner(routePlanner).build();
				}
				System.out.println("- Connecting to: " + urlMap.get("url"));
				HttpGet httpget = new HttpGet(urlMap.get("url"));
				httpclient.execute(httpget);
				startTime = System.currentTimeMillis();
				CloseableHttpResponse response = httpclient.execute(httpget);
				elapseTime = System.currentTimeMillis() - startTime;
				System.out.println("- Preparing to save url information");
				insertString = "INSERT INTO URL_INFORMATION(URL, STATUS_CODE, REASON_PHRASE, PROTOCOL_VERSION, HEADERS, LATENCY_MILLISECONDS, RESPONSE, DATE_TIME,IS_SERVER)VALUES(?,?,?,?,?,?,?,?,?)";
				PreparedStatement insertStm = Conn.prepareStatement(insertString);

				insertStm.setString(1, urlMap.get("url"));
				insertStm.setInt(2, response.getStatusLine().getStatusCode());
				insertStm.setString(3, response.getStatusLine().getReasonPhrase());
				insertStm.setString(4, response.getStatusLine().getProtocolVersion().toString());
				insertStm.setNString(5, response.getAllHeaders().toString());
				insertStm.setFloat(6, elapseTime);
				insertStm.setString(7, response.toString());
				insertStm.setTimestamp(8, sqlDate);
				insertStm.setString(9, urlMap.get("isServer"));
				insertStm.executeUpdate();
				System.out.println("- Url information saved successfully");
				//send email notification
				msg = getEmailTemplate("success","body");
				msg = msg.replace("@URL", urlMap.get("url"));
				msg = msg.replace("@STATUSCODE", Integer.toString(response.getStatusLine().getStatusCode()));
				msg = msg.replace("@ELAPSETIME", Float.toString(elapseTime));
				html = html.concat(msg);
				
			}
			html = getEmailTemplate("success","header").concat(html);
			
			sendEmail(config.get("email_to"), config.get("email_subject"), html);
			System.out.println("4. Please check email enter in the config file to see a resume...");
			System.out.println("<-- Process End... -->");

		} catch (Exception e) {
			//create error file
			createCsvFile("UrlError.csv", e.toString() + " " + e.getMessage(), "", dateFormat.format(curDate.getTime()));
			//send error notification
			sendErrornotification(e.toString() + "/n" + e.getMessage(), config.get("email_subject"));
			System.out.println(e.toString() + "/n" + e.getMessage());
			//e.printStackTrace();

		}

	}

	private static long connectToSalesforce(String  username, String pws, String url, String proxyIp, int proxyPort)
	{
		long startTime;
		long elapseTime = 0;
		try {

    		final String USERNAME = username;
	        // This is only a sample. Hard coding passwords in source files is a bad practice.
	        final String PASSWORD = pws; 
	        final String loginurl = "https://login.salesforce.com/services/Soap/u/40.0";
	        try {
	            ConnectorConfig config = new ConnectorConfig();
	            config.setUsername(USERNAME);
	            config.setPassword(PASSWORD); 
	            config.setProxy(proxyIp, proxyPort);
	            config.setAuthEndpoint(loginurl);
//	            PartnerConnection connection = Connector.newConnection(config);
	            startTime = System.currentTimeMillis();;

	            HttpHost proxy = new HttpHost(proxyIp, proxyPort); // telus proxy
				DefaultProxyRoutePlanner routePlanner = new DefaultProxyRoutePlanner(proxy);
				CloseableHttpClient httpclient = HttpClients.custom().setRoutePlanner(routePlanner).build();
				HttpGet httpget = new HttpGet(url);
				httpclient.execute(httpget);
	 			//calculateting elapse time
	 			elapseTime = System.currentTimeMillis() - startTime;
	 			
	         } catch (Exception ce) {
	            ce.printStackTrace();
	         }
	        
			
    	}catch (Exception e) {
			
			e.printStackTrace();
		}
		
		return elapseTime;

	}

	/**
    |--------------------------------------------------------------------------
    |@method getUrls
    |--------------------------------------------------------------------------
    |@description get all urls set in the config file and return a list with all urls
    |@created  07/11/2017
    |@createdBy x188421
    |@exception
    |@return List<String>
    */
	private static List<Map<String, String>> getUrls()
	{
		List<Map<String, String>> urlList = new ArrayList<Map<String, String>>();
		
		try {

			File file = new File("urlConfig.xml");
			DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
			Document doc = dBuilder.parse(file);
			NodeList urlConfig = doc.getElementsByTagName("url");

			
			for (int i = 0; i < urlConfig.getLength(); i++) 
			{
				Node urlNode = urlConfig.item(i);
				if (urlNode.getNodeType() == Node.ELEMENT_NODE) 
				{
					Map<String, String> map = new HashMap<String, String>();
					Element urlElement = (Element) urlNode;
					map.put("url", urlNode.getTextContent());
					map.put("isServer", urlNode.getAttributes().getNamedItem("server").getNodeValue());
					urlList.add(map);
				}
			}
			
		}catch (Exception e) {
			e.printStackTrace();
			
		}

		return urlList;
		
	}
	
	/**
    |--------------------------------------------------------------------------
    |@method getConfigInfo
    |--------------------------------------------------------------------------
    |@description get database, email, and proxy config information set in the config file and return a key->value map
    |@created  07/11/2017
    |@createdBy x188421
    |@exception
    |@return Map<String, String>
    */
	private static Map<String, String> getConfigInfo()
	{
		Map<String, String> map = new HashMap<String, String>();

		try {
			File file = new File("urlConfig.xml");
			DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
			Document doc = dBuilder.parse(file);
			//database config information
			NodeList dbCongif = doc.getElementsByTagName("database");
			for (int i = 0; i < dbCongif.getLength(); i++) 
			{
				Node dbNode = dbCongif.item(i);
				if (dbNode.getNodeType() == Node.ELEMENT_NODE) 
				{
					Element dbElement = (Element) dbNode;
					map.put("db_hostname", dbElement.getElementsByTagName("db_hostname").item(0).getTextContent());
					map.put("db_port", dbElement.getElementsByTagName("db_port").item(0).getTextContent());
					map.put("db_servicename", dbElement.getElementsByTagName("db_servicename").item(0).getTextContent());
					map.put("db_user", dbElement.getElementsByTagName("db_user").item(0).getTextContent());
					map.put("db_pws", dbElement.getElementsByTagName("db_pws").item(0).getTextContent());
				}
			}
			//email config information
			NodeList emconfig = doc.getElementsByTagName("email");
			for (int i = 0; i < emconfig.getLength(); i++) 
			{
				Node emNode = emconfig.item(i);
				if (emNode.getNodeType() == Node.ELEMENT_NODE) 
				{
					Element emElement = (Element) emNode;
					map.put("email_subject", emElement.getElementsByTagName("subject").item(0).getTextContent());
					map.put("email_to", emElement.getElementsByTagName("to").item(0).getTextContent());
				}
			}
			//proxy config information
			NodeList proxyconfig = doc.getElementsByTagName("proxy");
			for (int i = 0; i < proxyconfig.getLength(); i++) 
			{
				Node proxyNode = proxyconfig.item(i);
				if (proxyNode.getNodeType() == Node.ELEMENT_NODE) 
				{
					Element proxyElement = (Element) proxyNode;
					map.put("proxy_ip", proxyElement.getElementsByTagName("ip").item(0).getTextContent());
					map.put("proxy_port", proxyElement.getElementsByTagName("port").item(0).getTextContent());				
				}
			}

			//salesforce config information
			NodeList sfconfig = doc.getElementsByTagName("salesforce_credentials");
			for (int i = 0; i < sfconfig.getLength(); i++) 
			{
				Node sfNode = sfconfig.item(i);
				if (sfNode.getNodeType() == Node.ELEMENT_NODE) 
				{
					Element sfElement = (Element) sfNode;
					map.put("sf_test_con", sfElement.getElementsByTagName("sf_test_con").item(0).getTextContent());
					map.put("sf_username", sfElement.getElementsByTagName("sf_username").item(0).getTextContent());				
					map.put("sf_pws", sfElement.getElementsByTagName("sf_pws").item(0).getTextContent());
					map.put("sf_security_token", sfElement.getElementsByTagName("sf_security_token").item(0).getTextContent());	
					map.put("sf_url", "https://na35.salesforce.com/setup/forcecomHomepage.apexp?setupid=ForceCom");			
				}
			}
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		
		return map;

	}
	
	/**
    |--------------------------------------------------------------------------
    |@method sendErrornotification
    |--------------------------------------------------------------------------
    |@description send error email notification
    |@created  07/11/2017
    |@createdBy x188421
    |@parameters String error message, String email to mails, String email subject
    |@return void
    */
	private static void sendErrornotification(String errormsg, String subject)
	{
		String msg;
		String to = "anamaria.rodriguezhernandez@telus.com,michael.fuccaro@telus.com,greg.lebel@telus.com";
		msg = getEmailTemplate("error","header");
		msg += getEmailTemplate("error","body");
		msg = msg.replace("@MSG", errormsg);
		sendEmail(to, subject, msg);

	}

	/**
    |--------------------------------------------------------------------------
    |@method sendEmail
    |--------------------------------------------------------------------------
    |@description send email notification with summary 
    |@created  07/11/2017
    |@createdBy x188421
    |@parameters String email to mails, String email subject, String email message
    |@return void
    */
	private static void sendEmail(String to, String sub, String msg)
	{

		//Get properties object    
		Properties props = new Properties();  
		props.put("mail.smtp.auth", "false");
		//Put below to false, if no https is needed
		props.put("mail.smtp.starttls.enable", "true");
		props.put("mail.smtp.host", "abmsg.tsl.telus.com");    
		//props.put("mail.smtp.port", "25");   


		//get Session   
		//Session session = Session.getInstance(props);    
		Session session = Session.getDefaultInstance(props);//,  

		try {    
			MimeMessage message = new MimeMessage(session); 
			message.setFrom(new InternetAddress("urlinformationprocess@telus.com")); 
			//verifying if there is more than one email in the to string
			if(to.split(",").length > 1){
				message.setRecipients(Message.RecipientType.TO,InternetAddress.parse(to, true));
			}else{
				message.addRecipient(Message.RecipientType.TO,new InternetAddress(to));     
			}

			message.setSubject(sub);    
			message.setContent(msg,"text/html");      
			//send message  
			Transport.send(message);    

		} catch (MessagingException e) {throw new RuntimeException(e);}    

	}

	/**
    |--------------------------------------------------------------------------
    |@method createCsvFile
    |--------------------------------------------------------------------------
    |@description creates .csv error fils in the project main directory 
    |@created  07/11/2017
    |@createdBy x188421
    |@parameters String filename, String error message, String url, String date
    |@return void
    */
	private static void createCsvFile(String fileName, String error, String url, String curDate)
	{
		final String NEW_LINE_SEPARATOR = "\n";
		final Object [] FILE_HEADER = {"Url","Error","Date"};
		FileWriter fileWriter = null;
		CSVPrinter csvFilePrinter = null;
		List<String> errorArr = new ArrayList<String>();
		//Create the CSVFormat object with "\n" as a record delimiter
		CSVFormat csvFileFormat = CSVFormat.DEFAULT.withRecordSeparator(NEW_LINE_SEPARATOR);
		try{
			File f = new File(fileName);
			if(!f.exists() && !f.isDirectory()) { 
				fileWriter = new FileWriter(fileName);
				csvFilePrinter = new CSVPrinter(fileWriter, csvFileFormat);
				csvFilePrinter.printRecord(FILE_HEADER);
			}else{
				fileWriter = new FileWriter(fileName, true);
				csvFilePrinter = new CSVPrinter(fileWriter, csvFileFormat);
				//System.out.println("EXIST");
			}
			
			errorArr.add(url);
			errorArr.add(error);
			errorArr.add(curDate.toString());
			csvFilePrinter.printRecord(errorArr);

		} catch (Exception e) {
			System.out.println("Error in CsvFileWriter !!!");
			e.printStackTrace();
		} finally {
			try {
				fileWriter.flush();
				fileWriter.close();
				csvFilePrinter.close();
			} catch (IOException e) {
				System.out.println("Error while flushing/closing fileWriter/csvPrinter !!!");
				//e.printStackTrace();
			}
		}      		
	}
	/**
    |--------------------------------------------------------------------------
    |@method getEmailTemplate
    |--------------------------------------------------------------------------
    |@description gets notifications templates from emailTemplate.xml config file 
    |@created  07/14/2017
    |@createdBy x188421
    |@parameters String tagname, String childName
    |@return void
    */
	private static String getEmailTemplate(String tagname, String childName)
	{
		String msg = "";
		try {
			File file = new File("urlConfig.xml");
			DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
			Document doc = dBuilder.parse(file);
			NodeList tempconfig = doc.getElementsByTagName(tagname);
			for (int i = 0; i < tempconfig.getLength(); i++) 
			{
				Node tempNode = tempconfig.item(i);
				if (tempNode.getNodeType() == Node.ELEMENT_NODE) 
				{
					Element tempElement = (Element) tempNode;
					msg = tempElement.getElementsByTagName(childName).item(0).getTextContent();
				}
			}
		}catch (Exception e) {
			e.printStackTrace();
		}

		return msg;
	}
}
